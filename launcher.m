close all; clc; clearvars;

% Object parameters
M = 0.5; % Cart mass
m = 0.2; % Pendulum mass
L = 0.3; % Distance to center of pendulum mass
b = 0.1; % Moving friction
g = 9.80665; % Earth acceleration
Tp = 0.01; % Sampling time

% Continous state matrices
A = [0, ((M+m)*g)/(M*L), b/(M*L), 0; 
     1,               0,       0, 0;
     0,        -(m*g)/M,    -b/M, 0;
     0,               0,       1, 0];
B = [-1/(M*L);
            0;
          1/M;
           0];
C = eye(4);
D = zeros(4,1);

% Discrete state matrices
ss_discr = c2d(ss(A, B, C, D), Tp);
[Ad, Bd, Cd, Dd] = ssdata(ss_discr);

% Beginnig conditions
thetaDot0 = 0;
theta0 = 1/6 * pi;
xDot0 = 0;
x0 = -2;
state0 = [thetaDot0 theta0 xDot0 x0]';

% Simulation parameters
simTime = 10;
global simStep; simStep = Tp;
t = (0:simStep:simTime)';

% LQR
Q = [25,  0,   0,   0;  % theta_dot
     0,  50,   0,   0;  % theta
     0,   0,   1,   0;  % x_dot
     0,   0,   0,  25]; % x
R = 0.1;
K_LQR = lqrd(A,B,Q,R,Tp);

% Noises
var_measurement_noise = 0.02^2; % variance of measurement noise
var_process_noise = 0.001^2; % variance of process noise

% Run symulation
disp('Start symulacji...');
sim('symulation.slx');
disp('Koniec symulacji');

% Draw plots
run draws;
run animation;