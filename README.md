# Control of Inverted Pendulum
![alt text](Project/Image.png)

## Building
Linux:
~~~{.sh}
git clone https://gitlab.com/mmoskala07/inverted_pendulum.git
~~~
Windows:
~~~{.sh}
https://gitlab.com/mmoskala07/inverted_pendulum/-/archive/master/inverted_pendulum-master.zip
~~~

## Executing
In Matlab run file `launcher.m`

## Paper
[Inverted Pendulum](Project/Inverted_Pendulum.pdf)

## Videos
Inside videos showing how different controllers cope with problem
![Linear Quadratic Gaussian Controller](Videos/LQG.mp4)

## Maintainer
Mateusz Moskała  
mmoskala07@gmail.com  
