
figure('NumberTitle', 'on', 'Name', 'Controllers');

% PID
subplot(3,1,1);
plot(t, PID(:,2)); hold on;
plot(t, PID(:,4)); grid on;
legend('theta [rad]','x [m]');
xlabel('time [s]');
title('PID')
% LQR
subplot(3,1,2);
plot(t, LQR(:,2)); hold on;
plot(t, LQR(:,4)); grid on;
legend('theta [rad]','x [m]');
xlabel('time [s]');
title('LQR')
% LQG
subplot(3,1,3);
plot(t, LQG(:,2)); hold on;
plot(t, LQG(:,4)); grid on;
legend('theta [rad]','x [m]');
xlabel('time [s]');
title('LQG')

figure('NumberTitle', 'on', 'Name', 'LQG vs LQR');

% theta
subplot(2,1,1);
plot(t, LQR(:,2)); hold on;
plot(t, LQG(:,2)); grid on;
legend('LQR','LQG');
ylabel('theta [rad]')
xlabel('time [s]')
title('Theta')
% x
subplot(2,1,2);
plot(t, LQR(:,4)); hold on;
plot(t, LQG(:,4)); grid on;
legend('LQR','LQG');
ylabel('x [m]')
xlabel('time [s]')
title('x')

figure('NumberTitle', 'on', 'Name', 'Kalman Filter effect');
plot(t, Kalman_before); hold on;
plot(t, Kalman_after, 'Linewidth', 1, 'color', 'red'); grid on;
legend('Input signal','Output signal');
ylabel('theta [rad]')
xlabel('time [s]')
title('Kalman Filtering on Theta')
