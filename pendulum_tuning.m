clc; clearvars;
% Object parameters
M = 0.5; % Cart mass
m = 0.2; % Pendulum mass
L = 0.3; % Distance to center of pendulum mass
b = 0.1; % Moving friction
g = 9.80665; % Earth acceleration
Tp = 0.01;

% Parametry regulatora
K = 100; % 100
Ti = 0.15; % 0.15
Td = 0.07; % 0.07

% Transmitancje
z = tf('z', Tp);
s = tf('s');
Gr = K*(1 + (Tp*z)/(Ti*(z-1)) + (Td*(z-1))/(Tp*z));
Gso = -s/(M*L*s^3 + b*L*s^2 - (M+m)*g*s - b*g);
Go = c2d(Gso, Tp, 'zoh');
Gotw = series(Gr, Go);
Gzam = feedback(Gotw, 1, 1);

% Po�o�enie biegun�w
poles = pole(Gzam)
rlocus(Gzam)
% Odpowied� skokowa
%step(Gzam); grid on;